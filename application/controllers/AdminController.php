<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('Account');
        $this->load->model('Pencari');
        $this->load->model('Pemilik');
        $this->load->model('Kost');
        $this->load->model('Reservasi');
        $this->load->helper(array('form', 'url'));
    }

    public function admin(){
        if($this->session->userdata('logged_in')==1 && $this->session->userdata('role')==3){
            $pencari = 'pencari';
            $data['pencari'] = $this->Pencari->getakun($pencari);
            $this->load->view('admin',$data);
        }
        else{
            $this->load->view('homepage');
        }
        
    }
    public function admin_pemilik(){
        if($this->session->userdata('logged_in')==1 && $this->session->userdata('role')==3){
            $pemilik = 'pemilik';
            $data['pemilik'] = $this->Pemilik->getakun($pemilik);
            $this->load->view('admin_pemilik',$data);
        }
        else{
            $this->load->view('homepage');
        }
        
    }
    public function admin_listkost(){
        if($this->session->userdata('logged_in')==1 && $this->session->userdata('role')==3){
            $kost = 'kost';
            $data['kost'] = $this->Kost->getkost($kost);
            $this->load->view('admin_listkost',$data);
        }
        else{
            $this->load->view('homepage');
        }
        
    }
     public function admin_reservasi(){
        if($this->session->userdata('logged_in')==1 && $this->session->userdata('role')==3){
            $reservasi = 'reservasi';
            $data['reservasi'] = $this->Reservasi->get_reservasi_admin($reservasi);
            $this->load->view('admin_reservasi',$data);
        }
        else{
            $this->load->view('homepage');
        }
        
    }
    public function delete_akun($username,$role)
	{
        if($role=='pencari'){
            $this->Pencari->delete_akun($username);
            $this->Account->delete_akun($username);
            redirect('AdminController/admin');
        }
        else if ($role=='pemilik'){
            $this->Pemilik->delete_akun($username);
            $this->Account->delete_akun($username);
            redirect('AdminController/admin_pemilik');
        }
		
	}
    
    public function delete_kost($kodekost)
	{
        $this->Kost->delete_kost($kodekost);
		redirect('AdminController/admin_listkost');
    }
  
}

?>