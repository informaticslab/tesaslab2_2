-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 22 Jan 2021 pada 13.39
-- Versi server: 10.1.37-MariaDB
-- Versi PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_impal`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `account`
--

CREATE TABLE `account` (
  `username` varchar(20) NOT NULL,
  `password` varchar(500) NOT NULL,
  `role` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `account`
--

INSERT INTO `account` (`username`, `password`, `role`) VALUES
('a', '0cc175b9c0f1b6a831c399e269772661', 1),
('admin', '21232f297a57a5a743894a0e4a801fc3', 3),
('aslab', 'ad931de51833f907eeee5f708a2e89d0', 1),
('b', '92eb5ffee6ae2fec3ad71c777531578f', 2),
('ddeewwaann222', 'd8578edf8458ce06fbc5bb76a58c5ca4', 2),
('dewan99', '4a3529f64e965d36063392b8af90749d', 2),
('dewan999', '4a3529f64e965d36063392b8af90749d', 1),
('esa123', 'f301e29f96d44e55410e4ea0f0c158f8', 2),
('pemilik', '58399557dae3c60e23c78606771dfa3d', 2),
('pencari', '86ecd2486b77ff40f3b16569a19a3dc2', 1),
('zuqi123', 'd563b890e31257a2bf44ee1bd30f4401', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `username` varchar(30) NOT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kost`
--

CREATE TABLE `kost` (
  `namakost` varchar(30) NOT NULL,
  `kodekost` char(5) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `fasilitas` varchar(200) NOT NULL,
  `harga` int(20) NOT NULL,
  `jenis` varchar(10) NOT NULL,
  `namapemilik` varchar(30) NOT NULL,
  `contact` varchar(30) NOT NULL,
  `foto` varchar(500) NOT NULL,
  `foto2` varchar(500) NOT NULL,
  `foto3` varchar(500) NOT NULL,
  `jumlahkamar` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kost`
--

INSERT INTO `kost` (`namakost`, `kodekost`, `alamat`, `fasilitas`, `harga`, `jenis`, `namapemilik`, `contact`, `foto`, `foto2`, `foto3`, `jumlahkamar`) VALUES
('heihei2', 'kkkd3', 'sukpur', 'ac', 8000000, 'Laki-Laki', 'b', '08572110989899', 'upload/inikost.jpg', 'upload/inikost1.jpg', 'upload/inikost2.jpg', 99),
('heihei', 'kkkd7', 'sukpur', 'ac', 800000, 'Laki-Laki', 'b', '08572110989899', 'upload/11.jpg', 'upload/21.jpg', 'upload/31.jpg', 99),
('kostsambi', 'ksb1', 'sukpur', 'ac,air,kamar', 10000000, 'Campur', 'dewan99', '08572110989899', 'upload/kos11.jpg', 'upload/kos21.jpg', 'upload/kos31.jpg', 99);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pemilik`
--

CREATE TABLE `pemilik` (
  `nama` varchar(30) NOT NULL,
  `noidentitas` char(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(500) NOT NULL,
  `norekening` varchar(30) NOT NULL,
  `contact` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pemilik`
--

INSERT INTO `pemilik` (`nama`, `noidentitas`, `email`, `username`, `password`, `norekening`, `contact`) VALUES
('drdrdtdrt', '1301174134', 'muhammaddewan@student.telkomun', 'b', '92eb5ffee6ae2fec3ad71c777531578f', '1301174134', '08572110989899'),
('awdawdwad', 'aa', 'dewan.satriakamal@yahoo.com', 'ddeewwaann222', 'd8578edf8458ce06fbc5bb76a58c5ca4', '12312921839', '333434'),
('Muhammad Dewan', '1301174134', 'dewan.satriakamal@gmail.com', 'dewan99', '4a3529f64e965d36063392b8af90749d', '1301174134', '08572110989899'),
('esa alfitra', '612448181', 'esagila@gmail.com', 'esa123', 'f301e29f96d44e55410e4ea0f0c158f8', '84192021', '0812373737373'),
('dewan', '1301174134', 'dewan.satriakamal@yahoo.com', 'pemilik', '58399557dae3c60e23c78606771dfa3d', '1301174134', '08572110989899');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pencari`
--

CREATE TABLE `pencari` (
  `nama` varchar(30) NOT NULL,
  `noidentitas` char(20) NOT NULL,
  `email` varchar(35) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(500) NOT NULL,
  `norekening` varchar(30) NOT NULL,
  `contact` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pencari`
--

INSERT INTO `pencari` (`nama`, `noidentitas`, `email`, `username`, `password`, `norekening`, `contact`) VALUES
('aslab', '1212121', 'dewan.satriakamal@yahoo.com', 'aslab', 'ad931de51833f907eeee5f708a2e89d0', '12312921839', '0812373737373'),
('dewan', '6876876786', 'dewan.satriakamal@yahoo.com', 'dewan999', '4a3529f64e965d36063392b8af90749d', '130117413411', '08572110989890'),
('dewan', '1301174134', 'dewan.satriakamal@yahoo.com', 'pencari', '86ecd2486b77ff40f3b16569a19a3dc2', '1301174134', '08572110989899'),
('zuqi zuqi', '1212121', 'dewan.satriakamal@yahoo.com', 'zuqi123', 'd563b890e31257a2bf44ee1bd30f4401', '13221321', '082121212121');

-- --------------------------------------------------------

--
-- Struktur dari tabel `reservasi`
--

CREATE TABLE `reservasi` (
  `no` int(10) NOT NULL,
  `username` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `kodekost` varchar(10) NOT NULL,
  `pemilik` varchar(20) NOT NULL,
  `jumlahkamar` int(10) NOT NULL,
  `nominalreservasi` int(20) NOT NULL,
  `foto` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `reservasi`
--

INSERT INTO `reservasi` (`no`, `username`, `email`, `kodekost`, `pemilik`, `jumlahkamar`, `nominalreservasi`, `foto`, `status`) VALUES
(22, 'dewan999', 'dewan.satriakamal@yahoo.com', 'ksb1', 'dewan99', 8, 833334, 'reservasi/bukti_pembayara1.jpg', 'true'),
(23, 'pencari', 'dewan.satriakamal@yahoo.com', 'kkkd3', 'b', 1, 666667, 'reservasi/daftar_akun.png', 'true'),
(24, 'zuqi123', 'dewan.satriakamal@yahoo.com', 'w123', 'esa123', 20, 1666667, 'reservasi/kopa_logo_5.png', 'true'),
(25, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'ksb1', 'dewan99', 7, 833334, 'reservasi/Rambu3.png', 'true'),
(26, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'kkkd7', 'b', 22, 66667, 'reservasi/Rambu1.png', 'true'),
(27, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'ksb1', 'dewan99', 6, 833334, 'reservasi/E-TILANG.png', 'true'),
(28, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'kkkd7', 'b', 21, 66667, 'reservasi/Rambu31.png', 'true'),
(29, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'kkkd7', 'b', 20, 66667, 'reservasi/E-TILANG1.png', 'true'),
(30, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'kkkd7', 'b', 19, 66667, 'reservasi/Menu1.png', 'true'),
(31, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'ksb1', 'dewan99', 5, 833334, 'reservasi/Menu3.png', 'true'),
(32, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'ksb1', 'dewan99', 4, 833334, 'reservasi/Rambu22.png', 'true'),
(33, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'ksb1', 'dewan99', 3, 833334, 'reservasi/Rambu23.png', 'true'),
(34, 'aslab', 'muhammad.dsatriakamal@gmail.co', 'ksb1', 'dewan99', 2, 833334, 'reservasi/Rambu32.png', 'true'),
(35, 'aslab', 'dewan.satriakamal@yahoo.com', 'kkkd7', 'b', 18, 66667, 'reservasi/Etika_berkendara.png', 'true'),
(36, 'aslab', 'dewan.satriakamal@yahoo.com', 'ksb1', 'dewan99', 1, 833334, 'reservasi/Rambu6.png', 'true'),
(37, 'aslab', 'dewan.satriakamal@yahoo.com', 'kkkd3', 'b', 99, 666667, 'reservasi/Rambu33.png', 'true');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `kost`
--
ALTER TABLE `kost`
  ADD PRIMARY KEY (`kodekost`);

--
-- Indeks untuk tabel `pemilik`
--
ALTER TABLE `pemilik`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `pencari`
--
ALTER TABLE `pencari`
  ADD PRIMARY KEY (`username`);

--
-- Indeks untuk tabel `reservasi`
--
ALTER TABLE `reservasi`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `reservasi`
--
ALTER TABLE `reservasi`
  MODIFY `no` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
